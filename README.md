# IQVIA - Programming Assignment
Follow the steps to run the application, developed on python 3.7.1 expect to
use the same

1. Open terminal and navigate to this folder
2. Add 'python' to PATH if not already
3. To build package run
    > python setup.py bdist_wheel
4. Install dependencies
    > pip install -r requirements.txt
5. Install contact app
    > pip install dist/*.whl
6. To start application server run command
    > python run.py
7. Start celery scheduler
   > celery -A tasks beat
8. Start celery worker to execute the task
   > celery -A tasks worker -B