"""
Unit tetst for EmailModel
"""

import unittest
from unittest import mock
from contact.models.email import EmailModel


class TestEmailModel(unittest.TestCase):
    def setUp(self):
        self._obj = mock.MagicMock(EmailModel)

    def test_update(self):
        self._obj.update()
        self._obj.update.assert_called_once()


if __name__ == "__main__":
    unittest.main()
