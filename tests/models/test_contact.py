"""
Unit tetst for ContactModel
Source: contact.models.contact
"""

import unittest
from unittest import mock
from contact.models.contact import ContactModel


class TestContactModel(unittest.TestCase):
    def setUp(self):
        self._obj = mock.MagicMock(ContactModel)

    def test_find_by_username(self):
        self._obj.find_by_username()
        self._obj.find_by_username.assert_called_once()

    def test_find_all(self):
        self._obj.find_all()
        self._obj.find_all.assert_called_once()

    def test_update(self):
        self._obj.update()
        self._obj.update.assert_called_once()

    def test_save_to_db(self):
        self._obj.save_to_db()
        self._obj.save_to_db.assert_called_once()

    def test_delete_from_db(self):
        self._obj.delete_from_db()
        self._obj.delete_from_db.assert_called_once()


if __name__ == "__main__":
    unittest.main()
