"""
Unit test for contact resource
Source: contact.resources.contact
"""

import json
import unittest
from contact.app import create_app
from contact.db import db


class TestContact(unittest.TestCase):
    def setUp(self):
        app = create_app()
        app.config['TESTING'] = True
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test.db"
        db.init_app(app)
        with app.app_context():
            db.drop_all()
            db.create_all()
        self.app = app.test_client()

    def test_post(self):
        ret = self.app.post('/contact/johnmac', data=json.dumps({
            "firstname": "John",
            "surname": "Mac",
            "email_ids": [
                {
                    "email": "johnm@gmail.com",
                    "description": "Home"
                },
                {
                    "email": "mjohn@company.com",
                    "description": "Work"
                }
                ],
            }),
            headers={'Accept': 'application/json',
                     'Content-Type': 'application/json'
                     })
        self.assertEqual(200, ret.status_code)

    def test_get(self):
        ret = self.app.get('/contact/johnmac',
                            headers={'Accept': 'application/json',
                                     'Content-Type': 'application/json'
                                     })
        self.assertTrue(ret.status_code in [200, 204])

    def test_get_all(self):
        ret = self.app.get('/contact',
                            headers={'Accept': 'application/json',
                                     'Content-Type': 'application/json'
                                     })
        self.assertTrue(ret.status_code in [200, 204])

    def test_put(self):
        ret = self.app.put('/contact/johnmac', data=json.dumps({
            "surname": "R Mac",
            "email_ids": [
                {
                    "email": "johnm123@gmail.com",
                    "description": "Home"
                },
                {
                    "email": "mjohn56@company.com",
                    "description": "Work"
                }
                ],
            }),
            headers={'Accept': 'application/json',
                     'Content-Type': 'application/json'
                     })
        self.assertTrue(ret.status_code in [200, 400])

    def test_delete(self):
        ret = self.app.delete('/contact/johnmac',
                              headers={'Accept': 'application/json',
                                       'Content-Type': 'application/json'
                                       })
        self.assertTrue(ret.status_code in [200, 400])


if __name__ == "__main__":
    unittest.main()
