"""
Contact Schema module
"""
from contact.ma import ma
from contact.models.contact import ContactModel
from contact.schemas.email import EmailSchema


class ContactSchema(ma.ModelSchema):
    email_ids = ma.Nested(EmailSchema, many=True)

    class Meta:
        model = ContactModel
        load_only = ("id",)
