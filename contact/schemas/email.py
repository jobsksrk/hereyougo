"""
Email Schema module
"""

from marshmallow import fields, validate
from contact.ma import ma
from contact.models.email import EmailModel


class EmailSchema(ma.ModelSchema):
    email = fields.Str(
        validate=validate.Email(error='Not a valid email address'),
    )

    class Meta:
        model = EmailModel
        fields = ("id", "description", "email")
        load_only = ("id",)
