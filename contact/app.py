'''
Flask app module
'''
from flask import Flask
from flask_restful import Api
from contact.db import db
from contact.ma import ma
from contact.resources.contact import Contact


def create_app():
    '''
    Create flask app
    '''
    app = Flask(__name__)
    init_config(app)
    register_resources(app)
    initialize_extensions(app)
    app.before_first_request(db.create_all)
    return app


def initialize_extensions(app):
    '''
    Initialise flask extension
    '''
    db.init_app(app)
    ma.init_app(app)


def init_config(app):
    '''
    Init configuration for flask
    '''
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///data.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


def register_resources(app):
    '''
    Register resource
    '''
    api = Api(app)
    api.add_resource(Contact, "/contact/<string:username>", "/contact")
