'''
Resource for Contact
'''
from flask_restful import Resource, request
from contact.models.contact import ContactModel
from contact.schemas.contact import ContactSchema
from marshmallow import ValidationError


#Http status code
OK = 200
CREATED = 201
NO_DATA = 204
BAD_REQUEST = 400


#Response message
CONTACT_NOT_FOUND = "Contact(s) not exist"
CONTACT_ALREADY_EXISTS = "A user with username '{}' already exist"
CONTACT_DELETED = "Contact '{}' deleted"
CONTACT_UPDATED = "Contact '{}' updated"
CONTACT_NOT_UPDATED = "Contact '{}' could not be updated"
CONTACT_NOT_CREATED = "Contact is not created"


#Schemas
contact_schema = ContactSchema()
contacts_schema = ContactSchema(many=True)


class Contact(Resource):
    @classmethod
    def get(cls, username=None):
        '''
        Get method for Contact Resource
        '''
        contact = ContactModel.find_by_username(username)
        if contact:
            return contact_schema.dump(contact) if username else \
                       contacts_schema.dumps(contact), OK

        return {"message": CONTACT_NOT_FOUND}, NO_DATA

    @classmethod
    def post(cls, username):
        '''
        Post method for Contact Resource
        '''
        contact = cls.make_contact(username=username)
        if contact:
            if ContactModel.find_by_username(contact.username):
                return {"message": CONTACT_ALREADY_EXISTS.format(
                    contact.username)}, BAD_REQUEST
            contact.save_to_db()

            return contact_schema.dump(contact), OK

        return {"message": CONTACT_NOT_CREATED}, CREATED

    @classmethod
    def put(cls, username):
        '''
        Put method for Contact Resource
        '''
        contact = ContactModel.find_by_username(username)
        if contact:
            update_json = request.get_json()
            contact_update = contact_schema.load(update_json, partial=True)
            contact.update(contact_update)
        else:
            contact = cls.make_contact(username)

        if contact:
            contact.save_to_db()
            return contact_schema.dump(contact), OK

        return {"message": CONTACT_NOT_UPDATED.format(username)}, BAD_REQUEST

    @classmethod
    def delete(cls, username):
        '''
        Delete method for Contact Resource
        '''
        contact = ContactModel.find_by_username(username)
        if contact:
            contact.delete_from_db()
            return {'message': CONTACT_DELETED.format(username)}, OK

        return {"message": CONTACT_NOT_FOUND}, BAD_REQUEST

    @classmethod
    def make_contact(cls, username=None):
        '''
        Create contact object
        '''
        contact_json = request.get_json()
        if username:
            contact_json['username'] = username
        try:
            contact = contact_schema.load(contact_json)
        except ValidationError as err:
            return None

        return contact
