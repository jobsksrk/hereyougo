"""
Module implements the celery task
"""
from datetime import datetime
import json
import random
import requests
import string
from celery import Celery


celery = Celery('tasks', broker='redis://localhost:6379/0')
url_base = 'http://127.0.0.1:9000'
url_contact = '{url_base}/{resource}'.format(url_base=url_base,
                                             resource='contact')
url_endpoint = '{url_contact}/{username}'


headers = {'Accept': 'application/json',
           'Content-Type': 'application/json'
           }


def generate_unique_string(num):
    """
    Generate random unique string
    """
    buf = [random.choice(string.ascii_lowercase) for it in
           range(num)]
    return ''.join(buf)


@celery.task
def add_user():
    """
    Add contact every 15 sec time interval
    """
    username = generate_unique_string(8)
    firstname = generate_unique_string(10)
    surname = generate_unique_string(10)
    url = url_endpoint.format(url_contact=url_contact, username=username)
    data = {"firstname": "{}".format(firstname.capitalize()),
            "surname": "{}".format(surname.capitalize()),
            "email_ids": [{"description": "Home",
                           "email": "{}@gmail.com".format(surname)
                          },
                          {"description": "Work",
                           "email": "{}@gmail.com".format(firstname)
                          }
                         ]
            }
    try:
        response = requests.post(url=url, headers=headers,
                                 json=data)
    except Exception as err:
        print(err)

    if response.status_code == 200:
        print("Contact added successfully")
    else:
        print("Contact not added - {}".format(response.status_code))


@celery.task
def delete_user():
    """
    Task to delete contact 1 min expiry time
    """
    response = requests.get(url=url_contact,
                            headers=headers)
    if response.status_code == 200:
        contacts = json.loads(response.json())
        for contact in contacts:
            dt_now = datetime.now()
            dt_contact = datetime.strptime(contact['created'][:-6],
                                           "%Y-%m-%dT%H:%M:%S.%f")
            total_secs = (dt_now-dt_contact).total_seconds()
            if total_secs > 60:
                url = url_endpoint.format(url_contact=url_contact,
                                          username=contact['username'])
                response = requests.delete(url=url,
                                           headers=headers)
                if response.status_code == 200:
                    print("Deleted the expired user '{}'".format(
                        contact['username']))


#Add time based celery task to beat scheduler
celery.conf.beat_schedule = {
    "add-user-15sec": {
        "task": "tasks.add_user",
        "schedule": 15.0
    },
    "delete-user-1min-expiry": {
        "task": "tasks.delete_user",
        "schedule": 15.0
    }
}



