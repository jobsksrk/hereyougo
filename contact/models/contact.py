"""
Contact Model module
"""

from datetime import datetime
from contact.db import db
from contact.models.email import EmailModel


class ContactModel(db.Model):
    __tablename__ = "contact"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    firstname = db.Column(db.String(80), nullable=False)
    surname = db.Column(db.String(80), nullable=False)
    created = db.Column(db.DateTime(timezone=True),
                        default=datetime.now)
    email_ids = db.relationship('EmailModel',
                                backref='contacts',
                                cascade="all, delete-orphan",
                                lazy=True)

    @classmethod
    def find_by_username(cls, name):
        '''
        Query contact by username
        '''
        return cls.query.filter_by(username=name).first() if name else \
            cls.find_all()

    @classmethod
    def find_all(cls):
        '''
        Query all contacts
        '''
        return cls.query.all()

    def update(self, other):
        '''
        Update the contact details
        '''
        for it in self.__table__.columns:
            value = getattr(other, it.name)
            if value:
                setattr(self, it.name, value)
        for other_email in other.email_ids:
            for self_email in self.email_ids:
                if self_email.description == other_email.description:
                    self_email.update(other_email)

    def save_to_db(self):
        '''
        Saving to database
        '''
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        '''
        Delete from database
        '''
        db.session.delete(self)
        db.session.commit()
