"""
Email Model module
"""

from contact.db import db


class EmailModel(db.Model):
    __tablename__ = "email"
    __table_args__ = (
        db.UniqueConstraint('id', 'description',
                            name='unique_description'),
    )

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(16), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'),
                           nullable=False)

    def update(self, other):
        '''
        Update email
        '''
        self.email = other.email
