#! /usr/bin/python

"""
Main app module to run the application
"""
from contact.app import create_app


app = create_app()


if __name__ == "__main__":
    app.run(port=9000, debug=True)
